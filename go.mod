module gitlab.com/chunks1016.coder/terraform-openshift-installer

go 1.12

require (
	github.com/ajeddeloh/go-json v0.0.0-20170920214419-6a2fe990e083 // indirect
	github.com/gophercloud/utils v0.0.0-20191115025210-6e51b8944d05 // indirect
	github.com/gregjones/httpcache v0.0.0-20190611155906-901d90724c79 // indirect
	github.com/hashicorp/terraform v0.12.16 // indirect
	github.com/hashicorp/terraform-plugin-sdk v1.3.0
	github.com/openshift/api v3.9.1-0.20190307154011-600a5519d1ec+incompatible // indirect
	github.com/openshift/cloud-credential-operator v0.0.0-20191119195036-475436e11a88 // indirect
	github.com/openshift/cluster-api v0.0.0-20191030113141-9a3a7bbe9258 // indirect
	//github.com/openshift/cluster-api v0.0.0-20190131134342-91fca585a85b // indirect
	github.com/openshift/cluster-api-provider-libvirt v0.2.0 // indirect
	github.com/openshift/installer v0.16.1
	github.com/openshift/machine-config-operator v3.11.0+incompatible // indirect
	github.com/peterbourgon/diskv v2.0.1+incompatible // indirect
	github.com/terraform-providers/terraform-provider-aws v1.60.0 // indirect
	github.com/terraform-providers/terraform-provider-ignition v1.1.0 // indirect
	github.com/terraform-providers/terraform-provider-openstack v1.24.0 // indirect
	github.com/vincent-petithory/dataurl v0.0.0-20191104211930-d1553a71de50 // indirect
	go4.org v0.0.0-20191010144846-132d2879e1e9 // indirect
	gopkg.in/AlecAivazis/survey.v1 v1.8.7 // indirect
	gopkg.in/ini.v1 v1.51.0 // indirect
	k8s.io/api v0.0.0-20191115135540-bbc9463b57e5 // indirect
	k8s.io/apimachinery v0.0.0-20191116203941-08e4eafd6d11 // indirect
	k8s.io/client-go v11.0.1-0.20190409021438-1a26190bd76a+incompatible
	k8s.io/utils v0.0.0-20191114200735-6ca3b61696b6 // indirect
	sigs.k8s.io/cluster-api v0.0.0-00010101000000-000000000000 // indirect
	sigs.k8s.io/cluster-api-provider-aws v0.4.5 // indirect
	sigs.k8s.io/cluster-api-provider-openstack v0.0.0-00010101000000-000000000000 // indirect
	sigs.k8s.io/controller-runtime v0.4.0 // indirect
)

replace github.com/openshift/machine-config-operator => ./custom-vendor/github.com/openshift/machine-config-operator

replace sigs.k8s.io/cluster-api-provider-aws => github.com/openshift/cluster-api-provider-aws v0.2.1-0.20190208174550-15fe4b4ea563

replace sigs.k8s.io/cluster-api-provider-openstack => github.com/openshift/cluster-api-provider-openstack v0.0.0-20191030154608-c14315ec7102

replace sigs.k8s.io/cluster-api => github.com/openshift/cluster-api v0.0.0-20190131134342-91fca585a85b

replace k8s.io/client-go => k8s.io/client-go v9.0.0+incompatible

replace k8s.io/api => k8s.io/api v0.0.0-20181026184759-d1dc89ebaebe

replace k8s.io/apimachinery => k8s.io/apimachinery v0.0.0-20180913025736-6dd46049f395

//replace github.com/coreos/ignition => github.com/coreos/ignition v0.0.0-20170904171445-ea573e121f72
replace github.com/coreos/ignition => github.com/coreos/ignition v0.33.0
