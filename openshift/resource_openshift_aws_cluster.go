package openshift

import (
	"github.com/openshift/installer/pkg/asset"
	"io/ioutil"
	"log"
	"os"
	"os/exec"

	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	//"github.com/openshift/installer/pkg/asset/installconfig"
	assetstore "github.com/openshift/installer/pkg/asset/store"
	targetassets "github.com/openshift/installer/pkg/asset/targets"
)

const installConfig = `
apiVersion: v1
baseDomain: ${domain}
compute:
- hyperthreading: Enabled
  name: worker
  platform: {}
  replicas: 0
controlPlane:
  hyperthreading: Enabled
  name: master
  platform: {}
  replicas: 3
metadata:
  creationTimestamp: null
  name: ${cluster_id}
networking:
  clusterNetwork:
  - cidr: 10.128.0.0/14
    hostPrefix: 23
  machineCIDR: 10.0.0.0/16
  networkType: OpenShiftSDN
  serviceNetwork:
  - 172.30.0.0/16
platform:
  aws:
    region: us-east-2
pullSecret: ${pull_secret}
sshKey: |
  ${ssh-public-key}
`

type clusterTarget struct {
	name   string
	assets []asset.WritableAsset
}

func resourceOpenShiftAwsCluster() *schema.Resource {
	log.Printf("[DEBUG] In declaration.")
	return &schema.Resource{
		Create: resourceOpenShiftAwsClusterCreate,
		Read:   resourceOpenShiftAwsClusterRead,
		Delete: resourceOpenShiftAwsClusterDelete,

		Schema: map[string]*schema.Schema{
			"cluster_name": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "ID or name of the OpenShift cluster",
				ForceNew:    true,
			},
			"openshift_installer_path": {
				Type:     schema.TypeString,
				Optional: true,
				Default:  "openshift-install",
				ForceNew: true,
			},
			"working_dir": {
				Type:     schema.TypeString,
				Optional: true,
				ForceNew: true,
			},
			"pull_secret": {
				Type:     schema.TypeString,
				Required: true,
				ForceNew: true,
			},
			"platform": {
				Type:     schema.TypeString,
				Required: true,
				ForceNew: true,
			},
			"base_domain": {
				Type:     schema.TypeString,
				Optional: true,
				ForceNew: true,
			},
			"region": {
				Type:     schema.TypeString,
				Optional: true,
				ForceNew: true,
			},
		},
	}
}

func resourceOpenShiftAwsClusterRead(d *schema.ResourceData, meta interface{}) error {

	// If the output doesn't exist, mark the resource for creation.
	//if _, err := os.Stat(destinationDir); os.IsNotExist(err) {
	//	d.SetId("")
	//	return nil
	//}
	// ocpInstallerLoc := d.Get("openshift_installer_path").(string)
	var err error
	var workingDirPath string
	//var workingDirFiles []os.FileInfo
	workingDirVal, isSet := d.GetOk("working_dir")
	workingDirPath = workingDirVal.(string)
	if !isSet {
		workingDirPath, err = ioutil.TempDir("", "openshift_installer")
		defer os.RemoveAll(workingDirPath)
		if err != nil {
			return err
		}
	} else {
		var err error
		workingDirFiles, err = ioutil.ReadDir(workingDirPath)
		if err != nil {
			return err
		}
	}

	log.Printf("[DEBUG] Creating temp directory: %s", workingDirPath)
	//log.Printf("[DEBUG] List of files found: %s", workingDirFiles)

	// assetStore, err := assetstore.NewStore(clusterStateDir)
	// d.Set("assets", assetStore)

	if err != nil {
		return err
	}

	return nil
}

func resourceOpenShiftAwsClusterCreate(d *schema.ResourceData, meta interface{}) error {

	//ocpInstallerLoc := d.Get("openshift_installer_path").(string)
	var err error
	var workingDirPath string
	workingDirVal, isSet := d.GetOk("working_dir")
	if !isSet {
		workingDirPath, err = ioutil.TempDir("", "openshift4-install")
		defer os.RemoveAll(workingDirPath)
	} else {
		workingDirPath = workingDirVal.(string)
		workingDirPath, err = ioutil.TempDir(workingDirPath, "openshift4-install")
	}

	defer os.RemoveAll(workingDirPath)
	if err != nil {
		return err
	}

	createClusterTarget := clusterTarget{
		name:   d.Get("cluster_name").(string),
		assets: targetassets.Cluster,
	}

	assetStore, err := assetstore.NewStore(workingDirPath)
	for _, currentAsset := range createClusterTarget.assets {
		err := assetStore.Fetch(currentAsset)
		if err != nil {
			log.Printf("[DEBUG] Failed to fetch %s", currentAsset.Name())
		}
	}

	if err != nil {
		return err
	}

	return nil
}

func resourceOpenShiftAwsClusterDelete(d *schema.ResourceData, meta interface{}) error {
	ocpInstallerLoc := d.Get("openshift_installer_path").(string)
	cmd := exec.Command(ocpInstallerLoc)
	clusterStateDir := d.Get("cluster_state_dir").(string)
	log.Printf("[DEBUG] Command: %s", cmd.Args)

	assetStore, err := assetstore.NewStore(clusterStateDir)
	d.Set("assets", assetStore)

	if err != nil {
		return err
	}

	return nil
}
