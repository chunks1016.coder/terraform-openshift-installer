package openshift

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
)

func Provider() *schema.Provider {
	return &schema.Provider{
		ResourcesMap: map[string]*schema.Resource{
			"openshift_aws_cluster": resourceOpenShiftAwsCluster(),
		},
		// ResourcesMap: map[string]*schema.Resource{
		// 	"openshift_cluster": schema.DataSourceResourceShim(
		// 		"openshift_cluster",
		// 		dataSourceOpenShiftCluster(),
		// 	),
		// },
	}
}
