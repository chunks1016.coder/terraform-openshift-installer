package main

import (
	"log"

	"github.com/hashicorp/terraform-plugin-sdk/plugin"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"
	openshift "gitlab.com/chunks1016.coder/terraform-openshift-installer/openshift"
)

func main() {
	log.Printf("[DEBUG] In openshift provider")
	plugin.Serve(&plugin.ServeOpts{
		ProviderFunc: func() terraform.ResourceProvider {
			return openshift.Provider()
		},
	})
}
